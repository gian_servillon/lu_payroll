# Generated by Django 2.1.1 on 2018-11-21 00:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payrolllist', '0039_auto_20181120_1014'),
    ]

    operations = [
        migrations.AddField(
            model_name='payroll',
            name='pagibig_loan',
            field=models.DecimalField(decimal_places=2, default=0.0, max_digits=7),
        ),
        migrations.AddField(
            model_name='payroll',
            name='sss_loan',
            field=models.DecimalField(decimal_places=2, default=0.0, max_digits=7),
        ),
    ]
