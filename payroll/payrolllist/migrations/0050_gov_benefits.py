# Generated by Django 2.1.7 on 2019-04-13 02:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('employee', '0071_auto_20190413_0234'),
        ('payrolllist', '0049_auto_20190403_1357'),
    ]

    operations = [
        migrations.CreateModel(
            name='Gov_benefits',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('days', models.DecimalField(decimal_places=2, max_digits=7)),
                ('deductions', models.DecimalField(decimal_places=2, max_digits=7)),
                ('employee', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='employee_ben', to='employee.Employee')),
            ],
        ),
    ]
