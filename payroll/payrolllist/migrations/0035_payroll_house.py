# Generated by Django 2.1.1 on 2018-11-05 10:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payrolllist', '0034_payroll_rf'),
    ]

    operations = [
        migrations.AddField(
            model_name='payroll',
            name='house',
            field=models.DecimalField(decimal_places=2, default=0.0, max_digits=7),
        ),
    ]
