# Generated by Django 2.1.1 on 2018-11-06 09:00

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('employee', '0026_auto_20181106_0337'),
    ]

    operations = [
        migrations.CreateModel(
            name='Employee_sss_loan',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('load_amount', models.DecimalField(decimal_places=2, max_digits=7, null=True)),
                ('rate_to_deduct', models.DecimalField(decimal_places=2, max_digits=7, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('status', models.BooleanField(default=False)),
                ('employee', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='employee_sss_loan', to='employee.Employee')),
            ],
        ),
        migrations.CreateModel(
            name='Employee_sssloan_contrib',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cut_off_date', models.DateField()),
                ('contribution_collected', models.DecimalField(decimal_places=2, max_digits=7)),
                ('employee', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='employee_sssload_contrib', to='employee.Employee')),
                ('sss_loan', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='employee_sssloan_company', to='employee.Employee_company_loan')),
            ],
        ),
    ]
