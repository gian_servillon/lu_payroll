# Generated by Django 2.1.1 on 2018-10-04 12:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('employee', '0015_auto_20181004_0628'),
    ]

    operations = [
        migrations.AddField(
            model_name='employee',
            name='gender',
            field=models.CharField(default='1', max_length=50),
        ),
    ]
