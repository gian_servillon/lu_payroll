# Generated by Django 2.1.1 on 2018-10-03 02:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('employee', '0004_remove_employee_hiring_details_resume'),
    ]

    operations = [
        migrations.CreateModel(
            name='Employee_resume',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('resume', models.FileField(blank=True, upload_to='')),
                ('employee', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='employee_resume', to='employee.Employee')),
            ],
        ),
    ]
