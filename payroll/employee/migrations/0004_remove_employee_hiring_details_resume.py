# Generated by Django 2.1.1 on 2018-10-02 14:55

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('employee', '0003_auto_20181002_0738'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='employee_hiring_details',
            name='resume',
        ),
    ]
