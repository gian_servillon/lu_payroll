from django import forms
from django.contrib.auth.models import User
from .models import Employee, Employee_preferences, Employment_record, Employee_hiring_details, Employee_resume, Employee_company_loan, Employee_uniform, Employee_medical, Employee_canteen, Employee_gatepass, Employee_vale, Employee_pagibig_loan, Employee_pagibigloan_contrib, Employee_sss_loan, Employee_sssloan_contrib, Employee_acceptance, Employee_return_to_work, Employee_leave_history, Employee_citizenship, Employee_picture, Employee_requirements, Employee_memo
from hrms.models import Company
class EmployeeAddForm(forms.ModelForm):
	date_of_birth = forms.DateField(widget=forms.DateInput(attrs={'class': 'datepicker'}))
	GENDER = CHOICES = (('', 'Choose gender'),('male', 'male'), ('female', 'female'))
	gender = forms.CharField(widget=forms.Select(choices=GENDER))

	# queryset= Employee_citizenship.objects.values_list('citizenship', flat=True).distinct()
	# CITIZENSHIP = [('', 'Choose citizenship')] + [(citizenship, citizenship) for citizenship in queryset]
	# citizenship = forms.CharField(widget=forms.Select(choices=CITIZENSHIP,attrs={'onchange': 'Hide()'}))

	def __init__(self, *args, **kwargs):
		super(EmployeeAddForm, self).__init__(*args, **kwargs)
		queryset= Employee_citizenship.objects.values_list('citizenship', flat=True).distinct()
		CITIZENSHIP = [('', 'Choose citizenship')] + [(citizenship, citizenship) for citizenship in queryset]
		self.fields['citizenship'] = forms.ChoiceField(choices=CITIZENSHIP,  widget=forms.Select(attrs={'onchange': 'Hide()'}))


	CIVIL_STATUS = CHOICES = (('', 'Choose status'),('single', 'single'), ('married', 'married'))
	civil_status = forms.CharField(widget=forms.Select(choices=CIVIL_STATUS))
	date_hired = forms.DateField(widget=forms.DateInput(attrs={'class': 'datepicker'}))
	contract_expiration = forms.DateField(widget=forms.DateInput(attrs={'class': 'datepicker'}))
	
	SSS_OPTION = CHOICES = (('0', 'Choose Sss Option'),
	  ('bracket', 'bracket'),
	  ('manual', 'manual'), )
	sss_option = forms.CharField(widget=forms.Select(choices=SSS_OPTION, attrs={'onchange': 'HideSSS()'}))
	

	USE_COM = CHOICES = (
	  ('', 'Choose Gov Deduction to Impelement'),
	  ('company_base_deductions', 'company_base_deductions'),
	  ('employee_base_deductions', 'employee_base_deductions'),
	)
	gov_deductions_to_implement = forms.CharField(widget=forms.Select(choices=USE_COM, attrs={'onchange': 'HideGovDeductions()'}))
	 
	SSS_BRACKET = CHOICES = (('0', 'Choose Bracket'),
	 ('80', 'below 2250.00'), 
	 ('100', '2250.00 - 2749.99'), 
	 ('120', '2750.00 - 3249.99'),
	 ('140', '3250.00- 3749.99'), 
	 ('160', '3750.00 - 4249.99'),
	 ('180', '4250.00 - 4749.99'), 
	 ('200', '4750.00 - 5249.99'),
	 ('220', '5250.00 - 5749.99'), 
	 ('240', '5750.00 - 6249.99'),
	 ('260', '6250.00 - 6749.99'), 
	 ('280', '6750.00 - 7249.99'),
	 ('300', '7250.00 - 7749.99'), 
	 ('320', '7750.00 - 8249.99'),
	 ('340', '8250.00 - 8749.99'), 
	 ('360', '8750.00 - 9249.99'),
	 ('380', '9250.00 - 9749.99'), 
	 ('400', '9750.00 - 10249.99'),
	 ('420', '10250.00 - 10749.99'), 
	 ('440', '10750.00 - 11249.99'),
	 ('460', '11250.00 - 11749.99'), 
	 ('480', '11750.00 - 12249.99'),
	 ('500', '12250.00 - 12749.99'), 
	 ('520', '12750.00 - 13249.99'),
	 ('540', '13250.00 - 13749.99'), 
	 ('560', '13750.00 - 14249.99'),
	 ('580', '14250.00 - 14749.99'), 
	 ('600', '14750.00 - 15249.99'),
	 ('620', '15250.00 - 15749.99'), 
	 ('640', '15750.00 - 16249.99'),
	 ('660', '16250.00 - 16749.99'),
	 ('680', '16750.00 - 17249.99'),
	 ('700', '17250.00 - 17749.99'),
	 ('720', '17750.00 - 18249.99'),
	 ('740', '18250.00 - 18749.99'),
	 ('760', '18750.00 - 19249.99'),
	 ('780', '19250.00 - 19749.99'),
	 ('800', '19750.00 and above'),
	 )
	sss_bracket = forms.CharField(widget=forms.Select(choices=SSS_BRACKET))

	sss_no = forms.CharField(max_length=10, min_length=10)
	pagibig_no = forms.CharField(max_length=10, min_length=10)
	philhealth_no = forms.CharField(max_length=12, min_length=12)

	class Meta:
		model = Employee
		fields = ['first_name', 'middle_name', 'last_name', 'emp_id','address', 'provincial_address', 'date_of_birth', 'date_hired', 'contract_expiration','gender','place_of_birth', 'civil_status','phone', 'company', 'gov_deductions_to_implement','sss_option' ,'sss_bracket','sss_value','pagibig_value','philhealth_value',  'sss_no', 'pagibig_no', 'philhealth_no', 'tin_no','citizenship', 'remarks' ]

class PreferencesForm(forms.ModelForm):
	class Meta:
		model = Employee_preferences
		exclude = ['employee']

class RecordsForm(forms.ModelForm):
	from_date = forms.DateField(widget=forms.DateInput(attrs={'class': 'datepicker'}))
	to_date = forms.DateField(widget=forms.DateInput(attrs={'class': 'datepicker'}))
	class Meta:
		model = Employment_record
		exclude = ['employee']

class HiringDetailsForm(forms.ModelForm):
	PAYMENT_METHOD_CHOICES = CHOICES = (('', 'Choose method'),('weekly', 'weekly'), ('semi-monthly', 'semi-monthly'), ('monthly', 'monthly'))
	payment_method = forms.CharField(widget=forms.Select(choices=PAYMENT_METHOD_CHOICES))
	class Meta:
		model = Employee_hiring_details
		exclude = ['employee']

class ResumeForm(forms.ModelForm):
	class Meta:
		model = Employee_resume
		exclude = ['employee']

class PictureForm(forms.ModelForm):
	picture = forms.FileField(
        label='Select a picture',
        help_text=''
    )
	class Meta:
		model = Employee_picture
		exclude = ['employee']

class CompanyLoanForm(forms.ModelForm):
	class Meta:
		model = Employee_company_loan
		exclude = ['employee', 'status']

class PagibigLoanForm(forms.ModelForm):
	class Meta:
		model = Employee_pagibig_loan
		exclude = ['employee', 'status']

class SssLoanForm(forms.ModelForm):
	class Meta:
		model = Employee_sss_loan
		exclude = ['employee', 'status']


class UniformForm(forms.ModelForm):
	class Meta:
		model = Employee_uniform
		exclude = ['employee', 'status']

class MedicalForm(forms.ModelForm):
	class Meta:
		exclude = ['employee', 'status']
		model = Employee_medical

class CanteenForm(forms.ModelForm):
	class Meta:
		model = Employee_canteen
		exclude = ['employee', 'status']

class GatepassForm(forms.ModelForm):
	class Meta:
		model = Employee_gatepass
		exclude = ['employee', 'status']


class ValeForm(forms.ModelForm):
	class Meta:
		model = Employee_vale
		exclude = ['employee', 'status']


class SearchForm(forms.ModelForm):
	company_name = forms.CharField(label='',
		  widget= forms.TextInput(attrs={'placeholder':'Company'}), required=False)

	class Meta:
		model = Company
		fields = ['company_name']

class EmployeeAcceptanceForm(forms.ModelForm):
	employment_status = CHOICES = (('', 'Choose status'),('Casual / Probationary', 'Casual / Probationary'), ('Seasonal', 'Seasonal'), ('Regular', 'Regular'))
	employment_status = forms.CharField(widget=forms.Select(choices=employment_status))
	start_date = forms.DateField(widget=forms.DateInput(attrs={'class': 'datepicker'}))
	end_date = forms.DateField(widget=forms.DateInput(attrs={'class': 'datepicker'}))
	class Meta:
		model = Employee_acceptance
		fields = ['employment_status', 'start_date', 'end_date', 'position', 'salary_per_day', 'salary_per_month']

class EmployeeReturnToWork(forms.ModelForm):
	return_on = forms.DateField(widget=forms.DateInput(attrs={'class': 'datepicker'}))
	absence_date_start = forms.DateField(widget=forms.DateInput(attrs={'class': 'datepicker'}))
	absence_date_end = forms.DateField(widget=forms.DateInput(attrs={'class': 'datepicker'}))
	class Meta:
		model = Employee_return_to_work
		fields = ['return_on', 'absence_date_start', 'absence_date_end', 'reason']


class EmployeeLeaveHistory(forms.ModelForm):
	start_date = forms.DateField(widget=forms.DateInput(attrs={'class': 'datepicker'}))
	end_date = forms.DateField(widget=forms.DateInput(attrs={'class': 'datepicker'}))
	class Meta:
		model = Employee_leave_history
		fields = ['start_date', 'end_date', 'no_of_days']

class UpdateRecord(forms.ModelForm):
	from_date = forms.DateField(widget=forms.DateInput(attrs={'class': 'datepicker'}))
	to_date = forms.DateField(widget=forms.DateInput(attrs={'class': 'datepicker'}))
	class Meta:
		model = Employment_record
		exclude = ['employee']

class Requirements(forms.ModelForm):
	class Meta:
		model = Employee_requirements
		exclude = ['employee']

class MemoForm(forms.ModelForm):
	class Meta:
		model = Employee_memo
		exclude = ['employee']