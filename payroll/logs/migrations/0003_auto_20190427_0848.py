# Generated by Django 2.1.7 on 2019-04-27 08:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('logs', '0002_logs_employee'),
    ]

    operations = [
        migrations.AlterField(
            model_name='logs',
            name='action',
            field=models.CharField(max_length=255),
        ),
    ]
