# Generated by Django 2.1.1 on 2018-10-16 01:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hrms', '0023_auto_20181015_0441'),
    ]

    operations = [
        migrations.AddField(
            model_name='company_rates',
            name='activate_1_25_overtime',
            field=models.BooleanField(default=False),
        ),
    ]
