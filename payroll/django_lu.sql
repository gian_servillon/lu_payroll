-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 27, 2018 at 02:08 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `django_lu`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can view log entry', 1, 'view_logentry'),
(5, 'Can add permission', 2, 'add_permission'),
(6, 'Can change permission', 2, 'change_permission'),
(7, 'Can delete permission', 2, 'delete_permission'),
(8, 'Can view permission', 2, 'view_permission'),
(9, 'Can add group', 3, 'add_group'),
(10, 'Can change group', 3, 'change_group'),
(11, 'Can delete group', 3, 'delete_group'),
(12, 'Can view group', 3, 'view_group'),
(13, 'Can add user', 4, 'add_user'),
(14, 'Can change user', 4, 'change_user'),
(15, 'Can delete user', 4, 'delete_user'),
(16, 'Can view user', 4, 'view_user'),
(17, 'Can add content type', 5, 'add_contenttype'),
(18, 'Can change content type', 5, 'change_contenttype'),
(19, 'Can delete content type', 5, 'delete_contenttype'),
(20, 'Can view content type', 5, 'view_contenttype'),
(21, 'Can add session', 6, 'add_session'),
(22, 'Can change session', 6, 'change_session'),
(23, 'Can delete session', 6, 'delete_session'),
(24, 'Can view session', 6, 'view_session'),
(25, 'Can add company', 7, 'add_company'),
(26, 'Can change company', 7, 'change_company'),
(27, 'Can delete company', 7, 'delete_company'),
(28, 'Can view company', 7, 'view_company'),
(29, 'Can add employee_philhealth', 8, 'add_employee_philhealth'),
(30, 'Can change employee_philhealth', 8, 'change_employee_philhealth'),
(31, 'Can delete employee_philhealth', 8, 'delete_employee_philhealth'),
(32, 'Can view employee_philhealth', 8, 'view_employee_philhealth'),
(33, 'Can add employee_company_loan', 9, 'add_employee_company_loan'),
(34, 'Can change employee_company_loan', 9, 'change_employee_company_loan'),
(35, 'Can delete employee_company_loan', 9, 'delete_employee_company_loan'),
(36, 'Can view employee_company_loan', 9, 'view_employee_company_loan'),
(37, 'Can add employee_pagibig', 10, 'add_employee_pagibig'),
(38, 'Can change employee_pagibig', 10, 'change_employee_pagibig'),
(39, 'Can delete employee_pagibig', 10, 'delete_employee_pagibig'),
(40, 'Can view employee_pagibig', 10, 'view_employee_pagibig'),
(41, 'Can add employee_sss', 11, 'add_employee_sss'),
(42, 'Can change employee_sss', 11, 'change_employee_sss'),
(43, 'Can delete employee_sss', 11, 'delete_employee_sss'),
(44, 'Can view employee_sss', 11, 'view_employee_sss'),
(45, 'Can add employee_hiring_details', 12, 'add_employee_hiring_details'),
(46, 'Can change employee_hiring_details', 12, 'change_employee_hiring_details'),
(47, 'Can delete employee_hiring_details', 12, 'delete_employee_hiring_details'),
(48, 'Can view employee_hiring_details', 12, 'view_employee_hiring_details'),
(49, 'Can add employment_record', 13, 'add_employment_record'),
(50, 'Can change employment_record', 13, 'change_employment_record'),
(51, 'Can delete employment_record', 13, 'delete_employment_record'),
(52, 'Can view employment_record', 13, 'view_employment_record'),
(53, 'Can add employee', 14, 'add_employee'),
(54, 'Can change employee', 14, 'change_employee'),
(55, 'Can delete employee', 14, 'delete_employee'),
(56, 'Can view employee', 14, 'view_employee'),
(57, 'Can add employee_comload_contrib', 15, 'add_employee_comload_contrib'),
(58, 'Can change employee_comload_contrib', 15, 'change_employee_comload_contrib'),
(59, 'Can delete employee_comload_contrib', 15, 'delete_employee_comload_contrib'),
(60, 'Can view employee_comload_contrib', 15, 'view_employee_comload_contrib'),
(61, 'Can add employee_preferences', 16, 'add_employee_preferences'),
(62, 'Can change employee_preferences', 16, 'change_employee_preferences'),
(63, 'Can delete employee_preferences', 16, 'delete_employee_preferences'),
(64, 'Can view employee_preferences', 16, 'view_employee_preferences'),
(65, 'Can add employee_company_loan', 17, 'add_employee_company_loan'),
(66, 'Can change employee_company_loan', 17, 'change_employee_company_loan'),
(67, 'Can delete employee_company_loan', 17, 'delete_employee_company_loan'),
(68, 'Can view employee_company_loan', 17, 'view_employee_company_loan'),
(69, 'Can add employee_comload_contrib', 18, 'add_employee_comload_contrib'),
(70, 'Can change employee_comload_contrib', 18, 'change_employee_comload_contrib'),
(71, 'Can delete employee_comload_contrib', 18, 'delete_employee_comload_contrib'),
(72, 'Can view employee_comload_contrib', 18, 'view_employee_comload_contrib'),
(73, 'Can add employee_preferences', 19, 'add_employee_preferences'),
(74, 'Can change employee_preferences', 19, 'change_employee_preferences'),
(75, 'Can delete employee_preferences', 19, 'delete_employee_preferences'),
(76, 'Can view employee_preferences', 19, 'view_employee_preferences'),
(77, 'Can add employee', 20, 'add_employee'),
(78, 'Can change employee', 20, 'change_employee'),
(79, 'Can delete employee', 20, 'delete_employee'),
(80, 'Can view employee', 20, 'view_employee'),
(81, 'Can add employee_hiring_details', 21, 'add_employee_hiring_details'),
(82, 'Can change employee_hiring_details', 21, 'change_employee_hiring_details'),
(83, 'Can delete employee_hiring_details', 21, 'delete_employee_hiring_details'),
(84, 'Can view employee_hiring_details', 21, 'view_employee_hiring_details'),
(85, 'Can add employee_pagibig', 22, 'add_employee_pagibig'),
(86, 'Can change employee_pagibig', 22, 'change_employee_pagibig'),
(87, 'Can delete employee_pagibig', 22, 'delete_employee_pagibig'),
(88, 'Can view employee_pagibig', 22, 'view_employee_pagibig'),
(89, 'Can add employee_sss', 23, 'add_employee_sss'),
(90, 'Can change employee_sss', 23, 'change_employee_sss'),
(91, 'Can delete employee_sss', 23, 'delete_employee_sss'),
(92, 'Can view employee_sss', 23, 'view_employee_sss'),
(93, 'Can add employee_philhealth', 24, 'add_employee_philhealth'),
(94, 'Can change employee_philhealth', 24, 'change_employee_philhealth'),
(95, 'Can delete employee_philhealth', 24, 'delete_employee_philhealth'),
(96, 'Can view employee_philhealth', 24, 'view_employee_philhealth'),
(97, 'Can add employment_record', 25, 'add_employment_record'),
(98, 'Can change employment_record', 25, 'change_employment_record'),
(99, 'Can delete employment_record', 25, 'delete_employment_record'),
(100, 'Can view employment_record', 25, 'view_employment_record'),
(101, 'Can add employee_resume', 26, 'add_employee_resume'),
(102, 'Can change employee_resume', 26, 'change_employee_resume'),
(103, 'Can delete employee_resume', 26, 'delete_employee_resume'),
(104, 'Can view employee_resume', 26, 'view_employee_resume'),
(105, 'Can add employee_comloan_contrib', 18, 'add_employee_comloan_contrib'),
(106, 'Can change employee_comloan_contrib', 18, 'change_employee_comloan_contrib'),
(107, 'Can delete employee_comloan_contrib', 18, 'delete_employee_comloan_contrib'),
(108, 'Can view employee_comloan_contrib', 18, 'view_employee_comloan_contrib'),
(109, 'Can add employee_canteen', 27, 'add_employee_canteen'),
(110, 'Can change employee_canteen', 27, 'change_employee_canteen'),
(111, 'Can delete employee_canteen', 27, 'delete_employee_canteen'),
(112, 'Can view employee_canteen', 27, 'view_employee_canteen'),
(113, 'Can add employee_medical', 28, 'add_employee_medical'),
(114, 'Can change employee_medical', 28, 'change_employee_medical'),
(115, 'Can delete employee_medical', 28, 'delete_employee_medical'),
(116, 'Can view employee_medical', 28, 'view_employee_medical'),
(117, 'Can add employee_uniform', 29, 'add_employee_uniform'),
(118, 'Can change employee_uniform', 29, 'change_employee_uniform'),
(119, 'Can delete employee_uniform', 29, 'delete_employee_uniform'),
(120, 'Can view employee_uniform', 29, 'view_employee_uniform'),
(121, 'Can add payroll', 30, 'add_payroll'),
(122, 'Can change payroll', 30, 'change_payroll'),
(123, 'Can delete payroll', 30, 'delete_payroll'),
(124, 'Can view payroll', 30, 'view_payroll'),
(125, 'Can add base_payroll', 31, 'add_base_payroll'),
(126, 'Can change base_payroll', 31, 'change_base_payroll'),
(127, 'Can delete base_payroll', 31, 'delete_base_payroll'),
(128, 'Can view base_payroll', 31, 'view_base_payroll'),
(129, 'Can add company_rates', 32, 'add_company_rates'),
(130, 'Can change company_rates', 32, 'change_company_rates'),
(131, 'Can delete company_rates', 32, 'delete_company_rates'),
(132, 'Can view company_rates', 32, 'view_company_rates'),
(133, 'Can add employee_gatepass', 33, 'add_employee_gatepass'),
(134, 'Can change employee_gatepass', 33, 'change_employee_gatepass'),
(135, 'Can delete employee_gatepass', 33, 'delete_employee_gatepass'),
(136, 'Can view employee_gatepass', 33, 'view_employee_gatepass'),
(137, 'Can add employee_vale', 34, 'add_employee_vale'),
(138, 'Can change employee_vale', 34, 'change_employee_vale'),
(139, 'Can delete employee_vale', 34, 'delete_employee_vale'),
(140, 'Can view employee_vale', 34, 'view_employee_vale'),
(141, 'Can add employee_pagibigloan_contrib', 35, 'add_employee_pagibigloan_contrib'),
(142, 'Can change employee_pagibigloan_contrib', 35, 'change_employee_pagibigloan_contrib'),
(143, 'Can delete employee_pagibigloan_contrib', 35, 'delete_employee_pagibigloan_contrib'),
(144, 'Can view employee_pagibigloan_contrib', 35, 'view_employee_pagibigloan_contrib'),
(145, 'Can add employee_pagibig_loan', 36, 'add_employee_pagibig_loan'),
(146, 'Can change employee_pagibig_loan', 36, 'change_employee_pagibig_loan'),
(147, 'Can delete employee_pagibig_loan', 36, 'delete_employee_pagibig_loan'),
(148, 'Can view employee_pagibig_loan', 36, 'view_employee_pagibig_loan'),
(149, 'Can add employee_sssloan_contrib', 37, 'add_employee_sssloan_contrib'),
(150, 'Can change employee_sssloan_contrib', 37, 'change_employee_sssloan_contrib'),
(151, 'Can delete employee_sssloan_contrib', 37, 'delete_employee_sssloan_contrib'),
(152, 'Can view employee_sssloan_contrib', 37, 'view_employee_sssloan_contrib'),
(153, 'Can add employee_sss_loan', 38, 'add_employee_sss_loan'),
(154, 'Can change employee_sss_loan', 38, 'change_employee_sss_loan'),
(155, 'Can delete employee_sss_loan', 38, 'delete_employee_sss_loan'),
(156, 'Can view employee_sss_loan', 38, 'view_employee_sss_loan');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(1, 'pbkdf2_sha256$120000$4WwJ4pj3BDnX$VOoNRKSndUdzXrjDNcWS/TLxudH5XaBXGxNxcHM0yv8=', '2018-11-21 01:22:47.300790', 1, 'bretservilon', '', '', 'bretservilon@gmail.com', 1, 1, '2018-09-28 07:02:50.094412');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_admin_log`
--

INSERT INTO `django_admin_log` (`id`, `action_time`, `object_id`, `object_repr`, `action_flag`, `change_message`, `content_type_id`, `user_id`) VALUES
(1, '2018-09-28 13:39:54.870950', '1', 'Rebisco', 1, '[{\"added\": {}}]', 7, 1),
(2, '2018-09-28 13:59:25.779204', '1', 'Rebisco', 2, '[]', 7, 1),
(3, '2018-09-28 13:59:44.004637', '1', 'Rebisco', 2, '[]', 7, 1),
(4, '2018-10-01 08:31:04.931167', '1', 'Employee object (1)', 1, '[{\"added\": {}}]', 14, 1),
(5, '2018-10-01 13:42:24.932788', '1', 'Employee_preferences object (1)', 1, '[{\"added\": {}}]', 16, 1),
(6, '2018-10-01 13:46:27.947464', '1', 'Juan dela Cruz', 2, '[{\"changed\": {\"fields\": [\"phone\"]}}]', 14, 1),
(7, '2018-10-01 13:49:53.209369', '1', 'Employee_hiring_details object (1)', 1, '[{\"added\": {}}]', 12, 1),
(8, '2018-10-01 15:34:36.221917', '1', 'Juan dela Cruz - Hiring Details', 2, '[{\"changed\": {\"fields\": [\"position\"]}}]', 12, 1),
(9, '2018-10-02 07:20:27.862142', '10', 'Winchell Naval Servillon', 3, '', 20, 1),
(10, '2018-10-02 07:20:27.894639', '9', 'Winchell Naval Servillon', 3, '', 20, 1),
(11, '2018-10-02 07:20:27.901542', '8', 'Winchell Naval Servillon', 3, '', 20, 1),
(12, '2018-10-02 07:20:27.931540', '7', 'Winchell Naval Servillon', 3, '', 20, 1),
(13, '2018-10-02 07:20:27.967905', '6', 'Winchell Naval Servillon', 3, '', 20, 1),
(14, '2018-10-02 07:20:27.974247', '5', 'Winchell Naval Servillon', 3, '', 20, 1),
(15, '2018-10-02 07:20:27.981121', '4', 'Jade Naval Servillon', 3, '', 20, 1),
(16, '2018-10-02 07:20:27.986889', '3', 'Gian lapuz Servillon', 3, '', 20, 1),
(17, '2018-10-02 07:20:27.992529', '2', 'Gian lapuz Servillon', 3, '', 20, 1),
(18, '2018-10-02 07:20:28.006667', '1', 'Gian lapuz Servillon', 3, '', 20, 1),
(19, '2018-10-02 07:37:41.210343', '11', 'Winchell Naval Servillon', 3, '', 20, 1),
(20, '2018-10-02 07:39:11.908618', '12', 'Winchell Naval Servillon', 3, '', 20, 1),
(21, '2018-10-02 12:14:46.362936', '10', 'Winchell Naval Servillon - Preferences', 2, '[{\"changed\": {\"fields\": [\"occupation\"]}}]', 19, 1),
(22, '2018-10-03 02:38:58.149877', '14', 'jawo sdfsdf superman', 1, '[{\"added\": {}}]', 20, 1),
(23, '2018-10-03 03:10:39.432487', '2', 'Winchell Naval Servillon - resume', 1, '[{\"added\": {}}]', 26, 1),
(24, '2018-10-03 03:20:32.375449', '1', 'jawo sdfsdf superman - resume', 2, '[{\"changed\": {\"fields\": [\"resume\"]}}]', 26, 1),
(25, '2018-10-03 03:30:42.509506', '1', 'jawo sdfsdf superman - resume', 2, '[{\"changed\": {\"fields\": [\"resume\"]}}]', 26, 1),
(26, '2018-10-03 03:30:54.161385', '1', 'jawo sdfsdf superman - resume', 2, '[{\"changed\": {\"fields\": [\"resume\"]}}]', 26, 1),
(27, '2018-10-03 07:24:54.929631', '1', 'Winchell Naval Servillon - sss', 1, '[{\"added\": {}}]', 23, 1),
(28, '2018-10-03 11:42:00.221551', '1', 'Winchell Naval Servillon - Company Loan', 3, '', 17, 1),
(29, '2018-10-03 11:42:09.136770', '2', 'jawo sdfsdf superman - Company Loan', 3, '', 17, 1),
(30, '2018-10-03 12:24:44.541544', '3', 'Winchell Naval Servillon - Company Loan', 2, '[{\"changed\": {\"fields\": [\"status\"]}}]', 17, 1),
(31, '2018-10-03 12:46:37.551927', '1', 'Winchell Naval Servillon - company load contrib', 1, '[{\"added\": {}}]', 18, 1),
(32, '2018-10-03 12:51:44.122650', '2', 'Winchell Naval Servillon 2 - company load contrib', 1, '[{\"added\": {}}]', 18, 1),
(33, '2018-10-04 03:35:01.310569', '1', 'Winchell Naval Servillon - Uniform', 2, '[{\"changed\": {\"fields\": [\"rate_to_deduct\"]}}]', 29, 1),
(34, '2018-10-06 01:15:12.312989', '18', 'Jerry segovia anim', 3, '', 20, 1),
(35, '2018-10-06 01:15:12.327238', '17', 'Jerry segovia anim', 3, '', 20, 1),
(36, '2018-10-06 01:15:12.336551', '16', 'Jerry segovia anim', 3, '', 20, 1),
(37, '2018-10-06 01:15:12.351239', '15', 'Jerry segovia anim', 3, '', 20, 1),
(38, '2018-10-15 04:41:45.747971', '6', 'Anime', 1, '[{\"added\": {}}]', 7, 1),
(39, '2018-10-15 08:23:09.361460', '2', 'Rebisco rates', 1, '[{\"added\": {}}]', 32, 1),
(40, '2018-11-20 02:26:09.472832', '6', 'Winchell Naval Servillon - Company Loan', 3, '', 17, 1),
(41, '2018-11-20 02:26:09.550884', '5', 'Winchell Naval Servillon - Company Loan', 3, '', 17, 1),
(42, '2018-11-20 02:26:09.600920', '4', 'Winchell Naval Servillon - Company Loan', 3, '', 17, 1),
(43, '2018-11-20 02:26:09.633038', '3', 'Winchell Naval Servillon - Company Loan', 3, '', 17, 1),
(44, '2018-11-20 07:23:32.085731', '1', 'Winchell Naval Servillon - Vale', 3, '', 34, 1),
(45, '2018-11-21 02:13:57.612806', '1', 'Winchell Naval Servillon - Pagibig Loan', 2, '[{\"changed\": {\"fields\": [\"rate_to_deduct\"]}}]', 36, 1);

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'admin', 'logentry'),
(3, 'auth', 'group'),
(2, 'auth', 'permission'),
(4, 'auth', 'user'),
(5, 'contenttypes', 'contenttype'),
(20, 'employee', 'employee'),
(27, 'employee', 'employee_canteen'),
(18, 'employee', 'employee_comloan_contrib'),
(17, 'employee', 'employee_company_loan'),
(33, 'employee', 'employee_gatepass'),
(21, 'employee', 'employee_hiring_details'),
(28, 'employee', 'employee_medical'),
(22, 'employee', 'employee_pagibig'),
(35, 'employee', 'employee_pagibigloan_contrib'),
(36, 'employee', 'employee_pagibig_loan'),
(24, 'employee', 'employee_philhealth'),
(19, 'employee', 'employee_preferences'),
(26, 'employee', 'employee_resume'),
(23, 'employee', 'employee_sss'),
(37, 'employee', 'employee_sssloan_contrib'),
(38, 'employee', 'employee_sss_loan'),
(29, 'employee', 'employee_uniform'),
(34, 'employee', 'employee_vale'),
(25, 'employee', 'employment_record'),
(7, 'hrms', 'company'),
(32, 'hrms', 'company_rates'),
(14, 'hrms', 'employee'),
(15, 'hrms', 'employee_comload_contrib'),
(9, 'hrms', 'employee_company_loan'),
(12, 'hrms', 'employee_hiring_details'),
(10, 'hrms', 'employee_pagibig'),
(8, 'hrms', 'employee_philhealth'),
(16, 'hrms', 'employee_preferences'),
(11, 'hrms', 'employee_sss'),
(13, 'hrms', 'employment_record'),
(31, 'payrolllist', 'base_payroll'),
(30, 'payrolllist', 'payroll'),
(6, 'sessions', 'session');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2018-09-28 07:00:45.703281'),
(2, 'auth', '0001_initial', '2018-09-28 07:00:47.291927'),
(3, 'admin', '0001_initial', '2018-09-28 07:00:48.009409'),
(4, 'admin', '0002_logentry_remove_auto_add', '2018-09-28 07:00:48.029879'),
(5, 'admin', '0003_logentry_add_action_flag_choices', '2018-09-28 07:00:48.041654'),
(6, 'contenttypes', '0002_remove_content_type_name', '2018-09-28 07:00:48.254485'),
(7, 'auth', '0002_alter_permission_name_max_length', '2018-09-28 07:00:48.629904'),
(8, 'auth', '0003_alter_user_email_max_length', '2018-09-28 07:00:48.778898'),
(9, 'auth', '0004_alter_user_username_opts', '2018-09-28 07:00:48.794082'),
(10, 'auth', '0005_alter_user_last_login_null', '2018-09-28 07:00:48.937927'),
(11, 'auth', '0006_require_contenttypes_0002', '2018-09-28 07:00:48.949344'),
(12, 'auth', '0007_alter_validators_add_error_messages', '2018-09-28 07:00:48.967036'),
(13, 'auth', '0008_alter_user_username_max_length', '2018-09-28 07:00:49.273409'),
(14, 'auth', '0009_alter_user_last_name_max_length', '2018-09-28 07:00:49.428415'),
(15, 'sessions', '0001_initial', '2018-09-28 07:00:49.851026'),
(16, 'hrms', '0001_initial', '2018-09-28 08:45:19.957767'),
(17, 'hrms', '0002_company_status', '2018-09-28 13:53:17.620819'),
(18, 'hrms', '0003_employee', '2018-10-01 05:41:13.819906'),
(19, 'hrms', '0004_employee_preferences', '2018-10-01 05:41:14.296151'),
(20, 'hrms', '0005_employment_record', '2018-10-01 05:41:14.467789'),
(21, 'hrms', '0006_employee_hiring_details', '2018-10-01 05:41:14.893298'),
(22, 'hrms', '0007_employee_company_loan_employee_sss', '2018-10-01 05:41:15.399623'),
(23, 'hrms', '0008_employee_comload_contrib_employee_pagibig_employee_philhealth', '2018-10-01 05:41:16.132179'),
(24, 'hrms', '0009_auto_20181001_1341', '2018-10-01 13:41:14.694170'),
(25, 'hrms', '0010_auto_20181001_1342', '2018-10-01 13:42:07.808818'),
(26, 'hrms', '0011_employee_phone', '2018-10-01 13:46:10.958792'),
(27, 'hrms', '0012_auto_20181001_1346', '2018-10-01 13:46:46.423679'),
(28, 'hrms', '0013_employee_hiring_details_position', '2018-10-01 15:33:17.583713'),
(29, 'hrms', '0014_auto_20181001_1533', '2018-10-01 15:33:33.938373'),
(30, 'hrms', '0015_auto_20181002_0228', '2018-10-02 02:28:11.151513'),
(31, 'employee', '0001_initial', '2018-10-02 02:28:14.516477'),
(32, 'employee', '0002_auto_20181002_0719', '2018-10-02 07:19:46.786680'),
(33, 'employee', '0003_auto_20181002_0738', '2018-10-02 07:38:56.906062'),
(34, 'employee', '0004_remove_employee_hiring_details_resume', '2018-10-02 14:55:05.754929'),
(35, 'employee', '0005_employee_resume', '2018-10-03 02:30:22.029038'),
(36, 'employee', '0006_auto_20181003_1141', '2018-10-03 11:41:08.036730'),
(37, 'employee', '0007_employee_canteen_employee_medical_employee_uniform', '2018-10-04 02:48:38.462436'),
(38, 'hrms', '0016_auto_20181004_0248', '2018-10-04 02:48:38.761600'),
(39, 'employee', '0008_auto_20181004_0329', '2018-10-04 03:29:26.178678'),
(40, 'employee', '0009_auto_20181004_0333', '2018-10-04 03:33:09.718265'),
(41, 'employee', '0010_auto_20181004_0335', '2018-10-04 03:35:24.109192'),
(42, 'employee', '0011_employee_hiring_details_payment_method', '2018-10-04 06:00:47.840948'),
(43, 'employee', '0012_auto_20181004_0600', '2018-10-04 06:01:01.525561'),
(44, 'employee', '0013_auto_20181004_0627', '2018-10-04 06:27:47.789347'),
(45, 'employee', '0014_auto_20181004_0628', '2018-10-04 06:28:24.435041'),
(46, 'employee', '0015_auto_20181004_0628', '2018-10-04 06:28:34.052411'),
(47, 'payrolllist', '0001_initial', '2018-10-04 06:40:13.394888'),
(48, 'employee', '0016_employee_gender', '2018-10-04 12:20:06.549324'),
(49, 'employee', '0017_auto_20181004_1220', '2018-10-04 12:20:15.743067'),
(50, 'payrolllist', '0002_auto_20181004_1351', '2018-10-04 13:51:10.365268'),
(51, 'payrolllist', '0003_auto_20181004_1351', '2018-10-04 13:51:30.531804'),
(52, 'payrolllist', '0004_payroll_remarks', '2018-10-05 01:25:16.230027'),
(53, 'payrolllist', '0005_auto_20181005_0222', '2018-10-05 02:22:56.391601'),
(54, 'employee', '0018_auto_20181006_0114', '2018-10-06 01:14:23.780252'),
(55, 'employee', '0019_auto_20181006_0115', '2018-10-06 01:15:24.348616'),
(56, 'payrolllist', '0005_auto_20181006_0223', '2018-10-06 02:23:42.835301'),
(57, 'payrolllist', '0006_auto_20181006_0223', '2018-10-06 02:23:49.499110'),
(58, 'payrolllist', '0007_payroll_employee', '2018-10-06 10:09:38.667447'),
(59, 'payrolllist', '0008_auto_20181006_1009', '2018-10-06 10:09:48.861649'),
(60, 'payrolllist', '0009_auto_20181006_1012', '2018-10-06 10:12:28.314422'),
(61, 'hrms', '0017_auto_20181008_0835', '2018-10-08 08:35:26.437670'),
(62, 'payrolllist', '0010_remove_payroll_cb', '2018-10-08 08:35:26.556320'),
(63, 'hrms', '0018_auto_20181008_0915', '2018-10-08 09:15:49.611676'),
(64, 'hrms', '0019_remove_company_activate_gov_deductions', '2018-10-09 02:03:08.390715'),
(65, 'hrms', '0020_auto_20181011_0814', '2018-10-11 08:14:37.454786'),
(66, 'hrms', '0021_auto_20181015_0355', '2018-10-15 03:55:31.233362'),
(67, 'hrms', '0022_auto_20181015_0440', '2018-10-15 04:40:58.253469'),
(68, 'hrms', '0023_auto_20181015_0441', '2018-10-15 04:41:39.788319'),
(69, 'hrms', '0024_company_rates_activate_1_25_overtime', '2018-10-16 01:43:02.689092'),
(70, 'employee', '0020_employee_gatepass', '2018-10-16 07:32:14.739000'),
(71, 'employee', '0021_auto_20181016_0732', '2018-10-16 07:32:52.098912'),
(72, 'employee', '0022_employee_vale', '2018-10-16 09:39:31.096062'),
(73, 'employee', '0023_auto_20181016_0954', '2018-10-16 09:54:06.202399'),
(74, 'hrms', '0025_auto_20181017_0405', '2018-10-17 04:05:15.781611'),
(75, 'payrolllist', '0011_auto_20181017_0405', '2018-10-17 04:05:17.365738'),
(76, 'payrolllist', '0012_auto_20181018_0759', '2018-10-18 07:59:34.065904'),
(77, 'payrolllist', '0013_auto_20181019_0259', '2018-10-19 02:59:59.800629'),
(78, 'payrolllist', '0014_auto_20181019_0339', '2018-10-19 03:39:48.868422'),
(79, 'payrolllist', '0015_auto_20181020_0621', '2018-10-20 06:22:04.234551'),
(80, 'payrolllist', '0016_auto_20181020_0622', '2018-10-20 06:22:40.233888'),
(81, 'payrolllist', '0017_auto_20181020_0635', '2018-10-20 06:35:54.296525'),
(82, 'payrolllist', '0018_auto_20181020_0636', '2018-10-20 06:36:21.746849'),
(83, 'payrolllist', '0019_auto_20181020_0644', '2018-10-20 06:44:18.637994'),
(84, 'payrolllist', '0020_auto_20181020_0649', '2018-10-20 06:49:50.709129'),
(85, 'payrolllist', '0021_auto_20181020_0651', '2018-10-20 06:51:44.374193'),
(86, 'payrolllist', '0022_auto_20181020_0652', '2018-10-20 06:52:42.964684'),
(87, 'payrolllist', '0023_auto_20181020_0827', '2018-10-20 08:27:50.978435'),
(88, 'payrolllist', '0024_auto_20181020_0836', '2018-10-20 08:36:11.155742'),
(89, 'payrolllist', '0025_auto_20181022_0219', '2018-10-22 02:19:54.068508'),
(90, 'payrolllist', '0026_auto_20181022_1049', '2018-10-22 10:49:11.290132'),
(91, 'payrolllist', '0027_auto_20181023_0306', '2018-10-23 03:06:43.231052'),
(92, 'payrolllist', '0028_auto_20181023_0319', '2018-10-23 03:20:00.551652'),
(93, 'payrolllist', '0029_auto_20181023_0320', '2018-10-23 03:20:55.693144'),
(94, 'payrolllist', '0030_auto_20181023_0321', '2018-10-23 03:21:43.953821'),
(95, 'payrolllist', '0031_auto_20181023_0327', '2018-10-23 03:27:45.519523'),
(96, 'hrms', '0026_company_rates_activate_service_fee', '2018-11-03 06:33:27.932591'),
(97, 'payrolllist', '0032_payroll_service_fee', '2018-11-03 06:55:08.133531'),
(98, 'hrms', '0027_company_rates_ecola_rate', '2018-11-05 01:53:45.323597'),
(99, 'hrms', '0028_company_rates_activate_tshirt', '2018-11-05 09:46:59.862499'),
(100, 'payrolllist', '0033_payroll_tshirt', '2018-11-05 09:46:59.965061'),
(101, 'hrms', '0029_company_rates_activate_rf', '2018-11-05 10:06:15.573115'),
(102, 'payrolllist', '0034_payroll_rf', '2018-11-05 10:07:48.897225'),
(103, 'hrms', '0030_company_rates_activate_house', '2018-11-05 10:11:14.369720'),
(104, 'payrolllist', '0035_payroll_house', '2018-11-05 10:11:14.492873'),
(105, 'hrms', '0031_company_rates_activate_misc', '2018-11-05 10:24:05.182511'),
(106, 'payrolllist', '0036_payroll_misc', '2018-11-05 10:24:05.313732'),
(107, 'employee', '0024_employee_pagibigloan_contrib', '2018-11-06 02:49:46.037116'),
(108, 'employee', '0025_employee_pagibig_loan', '2018-11-06 03:06:55.293807'),
(109, 'employee', '0026_auto_20181106_0337', '2018-11-06 03:37:50.958182'),
(110, 'hrms', '0032_company_rates_activate_pagibig_loan', '2018-11-06 03:37:51.033048'),
(111, 'hrms', '0033_company_rates_activate_sss_loan', '2018-11-06 08:58:10.253812'),
(112, 'employee', '0027_employee_sss_loan_employee_sssloan_contrib', '2018-11-06 09:00:36.138884'),
(113, 'employee', '0018_auto_20181005_1557', '2018-11-07 06:44:44.892511'),
(114, 'employee', '0019_auto_20181005_1559', '2018-11-07 06:44:45.059493'),
(115, 'employee', '0028_merge_20181107_1444', '2018-11-07 06:44:45.100613'),
(116, 'hrms', '0034_company_rates_activate_sunday', '2018-11-07 06:44:46.170260'),
(117, 'payrolllist', '0037_auto_20181107_1556', '2018-11-07 07:57:09.381882'),
(118, 'hrms', '0035_company_rates_activate_overtime', '2018-11-14 14:08:44.377666'),
(119, 'hrms', '0036_remove_company_rates_activate_rest_overtime', '2018-11-14 14:08:46.205549'),
(120, 'hrms', '0037_remove_company_rates_activate_special_overtime', '2018-11-14 14:08:49.599980'),
(121, 'hrms', '0038_company_rates_activate_tardiness', '2018-11-14 14:08:51.524473'),
(122, 'hrms', '0039_company_rates_activate_holiday', '2018-11-14 14:08:53.157719'),
(123, 'payrolllist', '0038_auto_20181108_1047', '2018-11-14 14:08:56.386352'),
(124, 'employee', '0029_employee_emp_id', '2018-11-14 23:43:28.579841'),
(125, 'employee', '0030_auto_20181115_0731', '2018-11-14 23:43:28.689212'),
(126, 'employee', '0031_employee_hiring_details_contract_expiration', '2018-11-15 00:24:52.576063'),
(127, 'employee', '0032_auto_20181119_1409', '2018-11-19 06:09:40.984686'),
(128, 'payrolllist', '0039_auto_20181120_1014', '2018-11-20 02:15:31.053149'),
(129, 'payrolllist', '0040_auto_20181121_0841', '2018-11-21 00:42:02.457036');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('0a78e9e7p4pwpn8lrkxpyo7ntuyd0dit', 'ZGI2NTIwZTdiM2NhMzBiOGE4YzUwYTNkNzQ5YmZlZWI0NjYzZmQ0Yjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIxYzY0NWZhY2QyYmUwNTU3ZjMzNjg2MmUzYzkyN2M1ZTFmN2U5OGFiIn0=', '2018-10-12 08:44:49.165654'),
('3cv669g7g9k4fjg4h0ykt4lu9jhsy2hu', 'ZGI2NTIwZTdiM2NhMzBiOGE4YzUwYTNkNzQ5YmZlZWI0NjYzZmQ0Yjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIxYzY0NWZhY2QyYmUwNTU3ZjMzNjg2MmUzYzkyN2M1ZTFmN2U5OGFiIn0=', '2018-12-04 02:25:46.311872'),
('4ktloka8oyimeex5z49cuph9ltvryikz', 'ZGI2NTIwZTdiM2NhMzBiOGE4YzUwYTNkNzQ5YmZlZWI0NjYzZmQ0Yjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIxYzY0NWZhY2QyYmUwNTU3ZjMzNjg2MmUzYzkyN2M1ZTFmN2U5OGFiIn0=', '2018-10-29 04:34:13.229325'),
('ri24kk0024swdt5yirzd7awv5530d4pr', 'ZGI2NTIwZTdiM2NhMzBiOGE4YzUwYTNkNzQ5YmZlZWI0NjYzZmQ0Yjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIxYzY0NWZhY2QyYmUwNTU3ZjMzNjg2MmUzYzkyN2M1ZTFmN2U5OGFiIn0=', '2018-12-05 01:22:47.365832'),
('zf8fz0xjqrhxb90z0r3a1df9buq6wx7q', 'MTgzZTg1OWM3MmI5NTA0YmU5NTNmNGJhNzMxY2YyYTM3ZDNhYWE2Mzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIxYzY0NWZhY2QyYmUwNTU3ZjMzNjg2MmUzYzkyN2M1ZTFmN2U5OGFiIiwic3RhcnRfZGF0ZSI6IjEwLzE2LzIwMTgiLCJlbmRfZGF0ZSI6IjEwLzMxLzIwMTgiLCJjb21wYW55IjoiMSIsInBheW1lbnRfbWV0aG9kIjoid2Vla2x5In0=', '2018-10-20 01:25:46.563444');

-- --------------------------------------------------------

--
-- Table structure for table `employee_employee`
--

CREATE TABLE `employee_employee` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `address` varchar(255) NOT NULL,
  `provincial_address` varchar(255) NOT NULL,
  `date_of_birth` date NOT NULL,
  `place_of_birth` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `company_id` int(11) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `contract_expiration` date DEFAULT NULL,
  `date_hired` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_employee`
--

INSERT INTO `employee_employee` (`id`, `first_name`, `middle_name`, `last_name`, `address`, `provincial_address`, `date_of_birth`, `place_of_birth`, `phone`, `company_id`, `gender`, `emp_id`, `contract_expiration`, `date_hired`) VALUES
(13, 'Winchell', 'Naval', 'Servillon', 'malabon', 'ncr', '2018-10-11', 'caloocan', '232332323', 1, 'female', 1, '2018-11-30', '2018-11-01'),
(14, 'jawo', 'sdfsdf', 'superman', 'sdfs', 'kjk', '2018-10-03', 'caloocan', '232323', 1, 'male', 2, '2018-11-30', '2018-01-01'),
(19, 'Jerry', 'segovia', 'anim', 'Navotas', 'navotas', '2018-10-17', 'caloocan', '3434', 2, 'male', 3, NULL, NULL),
(20, 'Pepe', 'cruz', 'Rizal', 'Calocan', 'Lagun', '2018-10-15', 'caloocan', '9898', 1, 'male', 4, NULL, NULL),
(21, 'andres', 'anim', 'bonifacio', 'val', 'val', '2018-10-01', 'caloocan', '787878', 1, 'male', 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employee_employee_canteen`
--

CREATE TABLE `employee_employee_canteen` (
  `id` int(11) NOT NULL,
  `amount` decimal(7,2) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `rate_to_deduct` decimal(7,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_employee_canteen`
--

INSERT INTO `employee_employee_canteen` (`id`, `amount`, `employee_id`, `created_at`, `status`, `rate_to_deduct`) VALUES
(1, '400.00', 13, '2018-10-04 05:28:56.505788', 0, '50.00');

-- --------------------------------------------------------

--
-- Table structure for table `employee_employee_comloan_contrib`
--

CREATE TABLE `employee_employee_comloan_contrib` (
  `id` int(11) NOT NULL,
  `cut_off_date` date NOT NULL,
  `contribution_collected` decimal(7,2) NOT NULL,
  `company_loan_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employee_employee_company_loan`
--

CREATE TABLE `employee_employee_company_loan` (
  `id` int(11) NOT NULL,
  `load_amount` decimal(7,2) DEFAULT NULL,
  `rate_to_deduct` decimal(7,2) DEFAULT NULL,
  `employee_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_employee_company_loan`
--

INSERT INTO `employee_employee_company_loan` (`id`, `load_amount`, `rate_to_deduct`, `employee_id`, `status`, `created_at`) VALUES
(7, '1000.00', '100.00', 13, 0, '2018-11-20 02:26:31.080503');

-- --------------------------------------------------------

--
-- Table structure for table `employee_employee_gatepass`
--

CREATE TABLE `employee_employee_gatepass` (
  `id` int(11) NOT NULL,
  `amount` decimal(7,2) NOT NULL,
  `rate_to_deduct` decimal(7,2) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `employee_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_employee_gatepass`
--

INSERT INTO `employee_employee_gatepass` (`id`, `amount`, `rate_to_deduct`, `created_at`, `status`, `employee_id`) VALUES
(1, '2000.00', '100.00', '2018-10-16 09:51:40.079462', 0, 13);

-- --------------------------------------------------------

--
-- Table structure for table `employee_employee_hiring_details`
--

CREATE TABLE `employee_employee_hiring_details` (
  `id` int(11) NOT NULL,
  `atm` varchar(255) NOT NULL,
  `sss` varchar(255) NOT NULL,
  `pagibig` varchar(255) NOT NULL,
  `philhealth` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `rate` decimal(7,2) DEFAULT NULL,
  `training_rate` decimal(7,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_employee_hiring_details`
--

INSERT INTO `employee_employee_hiring_details` (`id`, `atm`, `sss`, `pagibig`, `philhealth`, `position`, `employee_id`, `payment_method`, `rate`, `training_rate`) VALUES
(4, 'sdfsf', 'ss', '[ag[a[', '[jo;', 'ceo', 13, 'weekly', '550.00', '450.00'),
(5, '23432432s', 'ljljlksjfj', 'lsdfjsld', 'sdlfjs', 'sdfsdfjsldjflskj', 14, 'weekly', '600.00', '550.00'),
(6, '2343243fsdf', '23423432sdfsd', '565dfdvv', '2323', 'dgdfgdfg', 19, 'weekly', '500.00', '450.00'),
(7, 'sdfksdfk;lk', 'l;k;k32;k4;32k', ';lk3;4k3;lk4', ';lk3;lk43', 'marketing', 20, 'semi-monthly', '500.00', '400.00'),
(8, 'sfks;dkf;l', 'ki3i43i', 'oipfipsoid', 'sdfsdjf2234', 'marketing', 21, 'semi-monthly', '450.00', '400.00');

-- --------------------------------------------------------

--
-- Table structure for table `employee_employee_medical`
--

CREATE TABLE `employee_employee_medical` (
  `id` int(11) NOT NULL,
  `amount` decimal(7,2) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `rate_to_deduct` decimal(7,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_employee_medical`
--

INSERT INTO `employee_employee_medical` (`id`, `amount`, `employee_id`, `created_at`, `status`, `rate_to_deduct`) VALUES
(1, '500.00', 13, '2018-10-04 05:27:52.663661', 0, '100.00');

-- --------------------------------------------------------

--
-- Table structure for table `employee_employee_pagibig`
--

CREATE TABLE `employee_employee_pagibig` (
  `id` int(11) NOT NULL,
  `cut_off_date` date NOT NULL,
  `contribution_collected` decimal(7,2) NOT NULL,
  `employee_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employee_employee_pagibigloan_contrib`
--

CREATE TABLE `employee_employee_pagibigloan_contrib` (
  `id` int(11) NOT NULL,
  `cut_off_date` date NOT NULL,
  `contribution_collected` decimal(7,2) NOT NULL,
  `pagibig_loan_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employee_employee_pagibig_loan`
--

CREATE TABLE `employee_employee_pagibig_loan` (
  `id` int(11) NOT NULL,
  `load_amount` decimal(7,2) DEFAULT NULL,
  `rate_to_deduct` decimal(7,2) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `employee_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_employee_pagibig_loan`
--

INSERT INTO `employee_employee_pagibig_loan` (`id`, `load_amount`, `rate_to_deduct`, `created_at`, `status`, `employee_id`) VALUES
(1, '1000.00', '20.00', '2018-11-06 06:47:05.428473', 0, 13);

-- --------------------------------------------------------

--
-- Table structure for table `employee_employee_philhealth`
--

CREATE TABLE `employee_employee_philhealth` (
  `id` int(11) NOT NULL,
  `cut_off_date` date NOT NULL,
  `contribution_collected` decimal(7,2) NOT NULL,
  `employee_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employee_employee_preferences`
--

CREATE TABLE `employee_employee_preferences` (
  `id` int(11) NOT NULL,
  `spouse` varchar(255) DEFAULT NULL,
  `occupation` varchar(255) DEFAULT NULL,
  `spouse_address` varchar(255) DEFAULT NULL,
  `character_reference_name` varchar(255) NOT NULL,
  `character_reference_address` varchar(255) NOT NULL,
  `character_reference_phone` varchar(255) NOT NULL,
  `employee_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_employee_preferences`
--

INSERT INTO `employee_employee_preferences` (`id`, `spouse`, `occupation`, `spouse_address`, `character_reference_name`, `character_reference_address`, `character_reference_phone`, `employee_id`) VALUES
(10, 'Gian Servillon', 'Ceoss', 'Navotas', 'si gianssss', 'sa navotassssasdfasdf', '282 98 77', 13),
(11, NULL, NULL, NULL, '', '', '', 14),
(16, NULL, NULL, NULL, '', '', '', 19),
(17, NULL, NULL, NULL, '', '', '', 20),
(18, NULL, NULL, NULL, '', '', '', 21);

-- --------------------------------------------------------

--
-- Table structure for table `employee_employee_resume`
--

CREATE TABLE `employee_employee_resume` (
  `id` int(11) NOT NULL,
  `resume` varchar(100) NOT NULL,
  `employee_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_employee_resume`
--

INSERT INTO `employee_employee_resume` (`id`, `resume`, `employee_id`) VALUES
(1, 'resume/sample.xlsx', 14),
(2, 'resume/Udemy_Paid_Courses.xlsx', 13),
(3, '', 19),
(4, '', 20),
(5, '', 21);

-- --------------------------------------------------------

--
-- Table structure for table `employee_employee_sss`
--

CREATE TABLE `employee_employee_sss` (
  `id` int(11) NOT NULL,
  `cut_off_date` date NOT NULL,
  `contribution_collected` decimal(7,2) NOT NULL,
  `employee_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_employee_sss`
--

INSERT INTO `employee_employee_sss` (`id`, `cut_off_date`, `contribution_collected`, `employee_id`) VALUES
(1, '2018-10-03', '100.00', 13);

-- --------------------------------------------------------

--
-- Table structure for table `employee_employee_sssloan_contrib`
--

CREATE TABLE `employee_employee_sssloan_contrib` (
  `id` int(11) NOT NULL,
  `cut_off_date` date NOT NULL,
  `contribution_collected` decimal(7,2) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `sss_loan_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employee_employee_sss_loan`
--

CREATE TABLE `employee_employee_sss_loan` (
  `id` int(11) NOT NULL,
  `load_amount` decimal(7,2) DEFAULT NULL,
  `rate_to_deduct` decimal(7,2) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `employee_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_employee_sss_loan`
--

INSERT INTO `employee_employee_sss_loan` (`id`, `load_amount`, `rate_to_deduct`, `created_at`, `status`, `employee_id`) VALUES
(1, '500.00', '100.00', '2018-11-06 10:04:29.341251', 0, 13);

-- --------------------------------------------------------

--
-- Table structure for table `employee_employee_uniform`
--

CREATE TABLE `employee_employee_uniform` (
  `id` int(11) NOT NULL,
  `amount` decimal(7,2) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `rate_to_deduct` decimal(7,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_employee_uniform`
--

INSERT INTO `employee_employee_uniform` (`id`, `amount`, `status`, `employee_id`, `created_at`, `rate_to_deduct`) VALUES
(1, '500.00', 0, 13, '2018-10-04 03:29:42.943053', '100.00');

-- --------------------------------------------------------

--
-- Table structure for table `employee_employee_vale`
--

CREATE TABLE `employee_employee_vale` (
  `id` int(11) NOT NULL,
  `amount` decimal(7,2) NOT NULL,
  `rate_to_deduct` decimal(7,2) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `employee_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_employee_vale`
--

INSERT INTO `employee_employee_vale` (`id`, `amount`, `rate_to_deduct`, `created_at`, `status`, `employee_id`) VALUES
(2, '1000.00', '50.00', '2018-11-20 07:37:41.333782', 0, 13);

-- --------------------------------------------------------

--
-- Table structure for table `employee_employment_record`
--

CREATE TABLE `employee_employment_record` (
  `id` int(11) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `company` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  `employee_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_employment_record`
--

INSERT INTO `employee_employment_record` (`id`, `from_date`, `to_date`, `company`, `position`, `employee_id`) VALUES
(3, '2015-05-01', '2015-11-28', 'DCST', 'TEacher', 13),
(4, '2017-12-01', '2018-09-01', 'sm', 'staff', 21);

-- --------------------------------------------------------

--
-- Table structure for table `hrms_company`
--

CREATE TABLE `hrms_company` (
  `id` int(11) NOT NULL,
  `company_name` varchar(150) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `contact_person` varchar(100) NOT NULL,
  `fax` varchar(20) NOT NULL,
  `address` varchar(150) NOT NULL,
  `email` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hrms_company`
--

INSERT INTO `hrms_company` (`id`, `company_name`, `phone`, `contact_person`, `fax`, `address`, `email`, `status`) VALUES
(1, 'Rebisco', '2323', 'Jade Naval', '3434', 'Navotas', 'Jade@gmail.com', 1),
(2, 'Magnolia', '34989', 'Alvin', '3439849', '', 'mag@gmail.com', 1),
(6, 'Anime', '22323', 'Jerry Anim', '090909', '12121', 'anime@gmail.com', 1),
(7, 'SM', '232323', 'henry sy', '998', '', 'sm@gmail.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `hrms_company_rates`
--

CREATE TABLE `hrms_company_rates` (
  `id` int(11) NOT NULL,
  `base_rate` decimal(7,2) DEFAULT NULL,
  `base_training_rate` decimal(7,2) DEFAULT NULL,
  `activate_rates` tinyint(1) NOT NULL,
  `activate_ecola` tinyint(1) NOT NULL,
  `activate_canteen` tinyint(1) NOT NULL,
  `sss` decimal(7,2) DEFAULT NULL,
  `philhealth` decimal(7,2) DEFAULT NULL,
  `pagibig` decimal(7,2) DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `activate_company_loan` tinyint(1) NOT NULL,
  `activate_gatepass` tinyint(1) NOT NULL,
  `activate_medical` tinyint(1) NOT NULL,
  `activate_night_differential` tinyint(1) NOT NULL,
  `activate_pants` tinyint(1) NOT NULL,
  `activate_rest_day` tinyint(1) NOT NULL,
  `activate_special` tinyint(1) NOT NULL,
  `activate_thirteenth_month` tinyint(1) NOT NULL,
  `activate_sil` tinyint(1) NOT NULL,
  `activate_training_rate` tinyint(1) NOT NULL,
  `activate_uniform` tinyint(1) NOT NULL,
  `activate_vale` tinyint(1) NOT NULL,
  `activate_1_25_overtime` tinyint(1) NOT NULL,
  `activate_service_fee` tinyint(1) NOT NULL,
  `ecola_rate` decimal(7,2) DEFAULT NULL,
  `activate_tshirt` tinyint(1) NOT NULL,
  `activate_rf` tinyint(1) NOT NULL,
  `activate_house` tinyint(1) NOT NULL,
  `activate_misc` tinyint(1) NOT NULL,
  `activate_pagibig_loan` tinyint(1) NOT NULL,
  `activate_sss_loan` tinyint(1) NOT NULL,
  `activate_sunday` tinyint(1) NOT NULL,
  `activate_overtime` tinyint(1) NOT NULL,
  `activate_tardiness` tinyint(1) NOT NULL,
  `activate_holiday` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hrms_company_rates`
--

INSERT INTO `hrms_company_rates` (`id`, `base_rate`, `base_training_rate`, `activate_rates`, `activate_ecola`, `activate_canteen`, `sss`, `philhealth`, `pagibig`, `company_id`, `activate_company_loan`, `activate_gatepass`, `activate_medical`, `activate_night_differential`, `activate_pants`, `activate_rest_day`, `activate_special`, `activate_thirteenth_month`, `activate_sil`, `activate_training_rate`, `activate_uniform`, `activate_vale`, `activate_1_25_overtime`, `activate_service_fee`, `ecola_rate`, `activate_tshirt`, `activate_rf`, `activate_house`, `activate_misc`, `activate_pagibig_loan`, `activate_sss_loan`, `activate_sunday`, `activate_overtime`, `activate_tardiness`, `activate_holiday`) VALUES
(1, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, '550.00', '300.00', 1, 1, 1, '255.00', '25.00', '50.00', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, '10.00', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(3, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `payrolllist_base_payroll`
--

CREATE TABLE `payrolllist_base_payroll` (
  `id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `payment_method` varchar(50) NOT NULL,
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payrolllist_base_payroll`
--

INSERT INTO `payrolllist_base_payroll` (`id`, `start_date`, `end_date`, `payment_method`, `company_id`) VALUES
(50, '2018-11-01', '2018-11-13', 'weekly', 1);

-- --------------------------------------------------------

--
-- Table structure for table `payrolllist_payroll`
--

CREATE TABLE `payrolllist_payroll` (
  `id` int(11) NOT NULL,
  `regular_days` decimal(7,2) NOT NULL,
  `rate` decimal(7,2) NOT NULL,
  `regular_amount` decimal(7,2) NOT NULL,
  `training_days` decimal(7,2) NOT NULL,
  `training_rate` decimal(7,2) NOT NULL,
  `training_amount` decimal(7,2) NOT NULL,
  `ecola` decimal(7,2) NOT NULL,
  `holiday_regular_days` decimal(7,2) NOT NULL,
  `holiday_regular_amount` decimal(7,2) NOT NULL,
  `holiday_training_days` decimal(7,2) NOT NULL,
  `holiday_training_amount` decimal(7,2) NOT NULL,
  `overtime_regular` decimal(7,2) NOT NULL,
  `overtime_regular_amount` decimal(7,2) NOT NULL,
  `overtime_training_days` decimal(7,2) NOT NULL,
  `overtime_training_amount` decimal(7,2) NOT NULL,
  `gross` decimal(7,2) DEFAULT NULL,
  `tardiness_undertime_regular` decimal(7,2) NOT NULL,
  `tardiness_undertime_regular_amount` decimal(7,2) NOT NULL,
  `gatepass` decimal(7,2) NOT NULL,
  `uniform` decimal(7,2) NOT NULL,
  `medical` decimal(7,2) NOT NULL,
  `canteen` decimal(7,2) NOT NULL,
  `sss` decimal(7,2) DEFAULT NULL,
  `pagibig` decimal(7,2) DEFAULT NULL,
  `philhealth` decimal(7,2) DEFAULT NULL,
  `net_amount` decimal(50,20) NOT NULL,
  `remarks` tinyint(1) NOT NULL,
  `base_payroll_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `night_diff_amount` decimal(7,2) NOT NULL,
  `night_diff_days` decimal(7,2) NOT NULL,
  `pants` decimal(7,2) NOT NULL,
  `rest_amount` decimal(7,2) NOT NULL,
  `rest_days` decimal(7,2) NOT NULL,
  `sil` decimal(7,2) NOT NULL,
  `special_holiday_amount` decimal(7,2) NOT NULL,
  `special_holiday_days` decimal(7,2) NOT NULL,
  `special_holiday_overtime_amount` decimal(7,2) NOT NULL,
  `special_holiday_overtime` decimal(7,2) NOT NULL,
  `thirteenth_month` decimal(7,2) NOT NULL,
  `vale` decimal(7,2) NOT NULL,
  `rest_day_overtime_amount` decimal(7,2) NOT NULL,
  `rest_day_overtime` decimal(7,2) NOT NULL,
  `service_fee` decimal(7,2) NOT NULL,
  `tshirt` decimal(7,2) NOT NULL,
  `rf` decimal(7,2) NOT NULL,
  `house` decimal(7,2) NOT NULL,
  `misc` decimal(7,2) NOT NULL,
  `sunday` decimal(7,2) NOT NULL,
  `sunday_amount` decimal(7,2) NOT NULL,
  `sunday_nd` decimal(7,2) NOT NULL,
  `sunday_nd_amount` decimal(7,2) NOT NULL,
  `sunday_overtime` decimal(7,2) NOT NULL,
  `sunday_overtime_amount` decimal(7,2) NOT NULL,
  `holiday_overtime` decimal(7,2) NOT NULL,
  `holiday_overtime_amount` decimal(7,2) NOT NULL,
  `company_loan` decimal(7,2) NOT NULL,
  `pagibig_loan` decimal(7,2) NOT NULL,
  `sss_loan` decimal(7,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payrolllist_payroll`
--

INSERT INTO `payrolllist_payroll` (`id`, `regular_days`, `rate`, `regular_amount`, `training_days`, `training_rate`, `training_amount`, `ecola`, `holiday_regular_days`, `holiday_regular_amount`, `holiday_training_days`, `holiday_training_amount`, `overtime_regular`, `overtime_regular_amount`, `overtime_training_days`, `overtime_training_amount`, `gross`, `tardiness_undertime_regular`, `tardiness_undertime_regular_amount`, `gatepass`, `uniform`, `medical`, `canteen`, `sss`, `pagibig`, `philhealth`, `net_amount`, `remarks`, `base_payroll_id`, `employee_id`, `night_diff_amount`, `night_diff_days`, `pants`, `rest_amount`, `rest_days`, `sil`, `special_holiday_amount`, `special_holiday_days`, `special_holiday_overtime_amount`, `special_holiday_overtime`, `thirteenth_month`, `vale`, `rest_day_overtime_amount`, `rest_day_overtime`, `service_fee`, `tshirt`, `rf`, `house`, `misc`, `sunday`, `sunday_amount`, `sunday_nd`, `sunday_nd_amount`, `sunday_overtime`, `sunday_overtime_amount`, `holiday_overtime`, `holiday_overtime_amount`, `company_loan`, `pagibig_loan`, `sss_loan`) VALUES
(46, '1.00', '550.00', '550.00', '0.00', '450.00', '0.00', '10.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '255.00', '50.00', '25.00', '-40.00000000000000000000', 0, 50, 13, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '50.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '100.00', '20.00', '100.00'),
(47, '1.00', '600.00', '600.00', '0.00', '550.00', '0.00', '10.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '255.00', '50.00', '25.00', '280.00000000000000000000', 0, 50, 14, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indexes for table `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Indexes for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indexes for table `employee_employee`
--
ALTER TABLE `employee_employee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_employee_company_id_3816de20_fk_hrms_company_id` (`company_id`);

--
-- Indexes for table `employee_employee_canteen`
--
ALTER TABLE `employee_employee_canteen`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_employee_ca_employee_id_cf78806c_fk_employee_` (`employee_id`);

--
-- Indexes for table `employee_employee_comloan_contrib`
--
ALTER TABLE `employee_employee_comloan_contrib`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_employee_co_company_loan_id_3255d843_fk_employee_` (`company_loan_id`),
  ADD KEY `employee_employee_co_employee_id_bf395978_fk_employee_` (`employee_id`);

--
-- Indexes for table `employee_employee_company_loan`
--
ALTER TABLE `employee_employee_company_loan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_employee_co_employee_id_7d8fdd6c_fk_employee_` (`employee_id`);

--
-- Indexes for table `employee_employee_gatepass`
--
ALTER TABLE `employee_employee_gatepass`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_employee_ga_employee_id_42abaab5_fk_employee_` (`employee_id`);

--
-- Indexes for table `employee_employee_hiring_details`
--
ALTER TABLE `employee_employee_hiring_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_employee_hi_employee_id_a0dfd6fd_fk_employee_` (`employee_id`);

--
-- Indexes for table `employee_employee_medical`
--
ALTER TABLE `employee_employee_medical`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_employee_me_employee_id_eb66c21a_fk_employee_` (`employee_id`);

--
-- Indexes for table `employee_employee_pagibig`
--
ALTER TABLE `employee_employee_pagibig`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_employee_pa_employee_id_f2fd5d49_fk_employee_` (`employee_id`);

--
-- Indexes for table `employee_employee_pagibigloan_contrib`
--
ALTER TABLE `employee_employee_pagibigloan_contrib`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_employee_pa_employee_id_59643011_fk_employee_` (`employee_id`),
  ADD KEY `employee_employee_pa_pagibig_loan_id_34aa749e_fk_employee_` (`pagibig_loan_id`);

--
-- Indexes for table `employee_employee_pagibig_loan`
--
ALTER TABLE `employee_employee_pagibig_loan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_employee_pa_employee_id_9358baa5_fk_employee_` (`employee_id`);

--
-- Indexes for table `employee_employee_philhealth`
--
ALTER TABLE `employee_employee_philhealth`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_employee_ph_employee_id_546c3872_fk_employee_` (`employee_id`);

--
-- Indexes for table `employee_employee_preferences`
--
ALTER TABLE `employee_employee_preferences`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_employee_pr_employee_id_344fef7d_fk_employee_` (`employee_id`);

--
-- Indexes for table `employee_employee_resume`
--
ALTER TABLE `employee_employee_resume`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_employee_re_employee_id_409e9ece_fk_employee_` (`employee_id`);

--
-- Indexes for table `employee_employee_sss`
--
ALTER TABLE `employee_employee_sss`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_employee_ss_employee_id_7797bd5c_fk_employee_` (`employee_id`);

--
-- Indexes for table `employee_employee_sssloan_contrib`
--
ALTER TABLE `employee_employee_sssloan_contrib`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_employee_ss_employee_id_9150b58b_fk_employee_` (`employee_id`),
  ADD KEY `employee_employee_ss_sss_loan_id_25814c67_fk_employee_` (`sss_loan_id`);

--
-- Indexes for table `employee_employee_sss_loan`
--
ALTER TABLE `employee_employee_sss_loan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_employee_ss_employee_id_17d62c54_fk_employee_` (`employee_id`);

--
-- Indexes for table `employee_employee_uniform`
--
ALTER TABLE `employee_employee_uniform`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_employee_un_employee_id_b1c06dc4_fk_employee_` (`employee_id`);

--
-- Indexes for table `employee_employee_vale`
--
ALTER TABLE `employee_employee_vale`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_employee_va_employee_id_82de8239_fk_employee_` (`employee_id`);

--
-- Indexes for table `employee_employment_record`
--
ALTER TABLE `employee_employment_record`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_employment__employee_id_038d4d31_fk_employee_` (`employee_id`);

--
-- Indexes for table `hrms_company`
--
ALTER TABLE `hrms_company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hrms_company_rates`
--
ALTER TABLE `hrms_company_rates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hrms_company_rates_company_id_08aa257e_fk_hrms_company_id` (`company_id`);

--
-- Indexes for table `payrolllist_base_payroll`
--
ALTER TABLE `payrolllist_base_payroll`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payrolllist_base_payroll_company_id_c3b1740d_fk_hrms_company_id` (`company_id`);

--
-- Indexes for table `payrolllist_payroll`
--
ALTER TABLE `payrolllist_payroll`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payrolllist_payroll_base_payroll_id_66659f13_fk_payrollli` (`base_payroll_id`),
  ADD KEY `payrolllist_payroll_employee_id_9cc9eed8_fk_employee_employee_id` (`employee_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=157;

--
-- AUTO_INCREMENT for table `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;

--
-- AUTO_INCREMENT for table `employee_employee`
--
ALTER TABLE `employee_employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `employee_employee_canteen`
--
ALTER TABLE `employee_employee_canteen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `employee_employee_comloan_contrib`
--
ALTER TABLE `employee_employee_comloan_contrib`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employee_employee_company_loan`
--
ALTER TABLE `employee_employee_company_loan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `employee_employee_gatepass`
--
ALTER TABLE `employee_employee_gatepass`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `employee_employee_hiring_details`
--
ALTER TABLE `employee_employee_hiring_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `employee_employee_medical`
--
ALTER TABLE `employee_employee_medical`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `employee_employee_pagibig`
--
ALTER TABLE `employee_employee_pagibig`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employee_employee_pagibigloan_contrib`
--
ALTER TABLE `employee_employee_pagibigloan_contrib`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employee_employee_pagibig_loan`
--
ALTER TABLE `employee_employee_pagibig_loan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `employee_employee_philhealth`
--
ALTER TABLE `employee_employee_philhealth`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employee_employee_preferences`
--
ALTER TABLE `employee_employee_preferences`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `employee_employee_resume`
--
ALTER TABLE `employee_employee_resume`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `employee_employee_sss`
--
ALTER TABLE `employee_employee_sss`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `employee_employee_sssloan_contrib`
--
ALTER TABLE `employee_employee_sssloan_contrib`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employee_employee_sss_loan`
--
ALTER TABLE `employee_employee_sss_loan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `employee_employee_uniform`
--
ALTER TABLE `employee_employee_uniform`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `employee_employee_vale`
--
ALTER TABLE `employee_employee_vale`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `employee_employment_record`
--
ALTER TABLE `employee_employment_record`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `hrms_company`
--
ALTER TABLE `hrms_company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `hrms_company_rates`
--
ALTER TABLE `hrms_company_rates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `payrolllist_base_payroll`
--
ALTER TABLE `payrolllist_base_payroll`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `payrolllist_payroll`
--
ALTER TABLE `payrolllist_payroll`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `employee_employee`
--
ALTER TABLE `employee_employee`
  ADD CONSTRAINT `employee_employee_company_id_3816de20_fk_hrms_company_id` FOREIGN KEY (`company_id`) REFERENCES `hrms_company` (`id`);

--
-- Constraints for table `employee_employee_canteen`
--
ALTER TABLE `employee_employee_canteen`
  ADD CONSTRAINT `employee_employee_ca_employee_id_cf78806c_fk_employee_` FOREIGN KEY (`employee_id`) REFERENCES `employee_employee` (`id`);

--
-- Constraints for table `employee_employee_comloan_contrib`
--
ALTER TABLE `employee_employee_comloan_contrib`
  ADD CONSTRAINT `employee_employee_co_company_loan_id_3255d843_fk_employee_` FOREIGN KEY (`company_loan_id`) REFERENCES `employee_employee_company_loan` (`id`),
  ADD CONSTRAINT `employee_employee_co_employee_id_bf395978_fk_employee_` FOREIGN KEY (`employee_id`) REFERENCES `employee_employee` (`id`);

--
-- Constraints for table `employee_employee_company_loan`
--
ALTER TABLE `employee_employee_company_loan`
  ADD CONSTRAINT `employee_employee_co_employee_id_7d8fdd6c_fk_employee_` FOREIGN KEY (`employee_id`) REFERENCES `employee_employee` (`id`);

--
-- Constraints for table `employee_employee_gatepass`
--
ALTER TABLE `employee_employee_gatepass`
  ADD CONSTRAINT `employee_employee_ga_employee_id_42abaab5_fk_employee_` FOREIGN KEY (`employee_id`) REFERENCES `employee_employee` (`id`);

--
-- Constraints for table `employee_employee_hiring_details`
--
ALTER TABLE `employee_employee_hiring_details`
  ADD CONSTRAINT `employee_employee_hi_employee_id_a0dfd6fd_fk_employee_` FOREIGN KEY (`employee_id`) REFERENCES `employee_employee` (`id`);

--
-- Constraints for table `employee_employee_medical`
--
ALTER TABLE `employee_employee_medical`
  ADD CONSTRAINT `employee_employee_me_employee_id_eb66c21a_fk_employee_` FOREIGN KEY (`employee_id`) REFERENCES `employee_employee` (`id`);

--
-- Constraints for table `employee_employee_pagibig`
--
ALTER TABLE `employee_employee_pagibig`
  ADD CONSTRAINT `employee_employee_pa_employee_id_f2fd5d49_fk_employee_` FOREIGN KEY (`employee_id`) REFERENCES `employee_employee` (`id`);

--
-- Constraints for table `employee_employee_pagibigloan_contrib`
--
ALTER TABLE `employee_employee_pagibigloan_contrib`
  ADD CONSTRAINT `employee_employee_pa_employee_id_59643011_fk_employee_` FOREIGN KEY (`employee_id`) REFERENCES `employee_employee` (`id`),
  ADD CONSTRAINT `employee_employee_pa_pagibig_loan_id_34aa749e_fk_employee_` FOREIGN KEY (`pagibig_loan_id`) REFERENCES `employee_employee_company_loan` (`id`);

--
-- Constraints for table `employee_employee_pagibig_loan`
--
ALTER TABLE `employee_employee_pagibig_loan`
  ADD CONSTRAINT `employee_employee_pa_employee_id_9358baa5_fk_employee_` FOREIGN KEY (`employee_id`) REFERENCES `employee_employee` (`id`);

--
-- Constraints for table `employee_employee_philhealth`
--
ALTER TABLE `employee_employee_philhealth`
  ADD CONSTRAINT `employee_employee_ph_employee_id_546c3872_fk_employee_` FOREIGN KEY (`employee_id`) REFERENCES `employee_employee` (`id`);

--
-- Constraints for table `employee_employee_preferences`
--
ALTER TABLE `employee_employee_preferences`
  ADD CONSTRAINT `employee_employee_pr_employee_id_344fef7d_fk_employee_` FOREIGN KEY (`employee_id`) REFERENCES `employee_employee` (`id`);

--
-- Constraints for table `employee_employee_resume`
--
ALTER TABLE `employee_employee_resume`
  ADD CONSTRAINT `employee_employee_re_employee_id_409e9ece_fk_employee_` FOREIGN KEY (`employee_id`) REFERENCES `employee_employee` (`id`);

--
-- Constraints for table `employee_employee_sss`
--
ALTER TABLE `employee_employee_sss`
  ADD CONSTRAINT `employee_employee_ss_employee_id_7797bd5c_fk_employee_` FOREIGN KEY (`employee_id`) REFERENCES `employee_employee` (`id`);

--
-- Constraints for table `employee_employee_sssloan_contrib`
--
ALTER TABLE `employee_employee_sssloan_contrib`
  ADD CONSTRAINT `employee_employee_ss_employee_id_9150b58b_fk_employee_` FOREIGN KEY (`employee_id`) REFERENCES `employee_employee` (`id`),
  ADD CONSTRAINT `employee_employee_ss_sss_loan_id_25814c67_fk_employee_` FOREIGN KEY (`sss_loan_id`) REFERENCES `employee_employee_company_loan` (`id`);

--
-- Constraints for table `employee_employee_sss_loan`
--
ALTER TABLE `employee_employee_sss_loan`
  ADD CONSTRAINT `employee_employee_ss_employee_id_17d62c54_fk_employee_` FOREIGN KEY (`employee_id`) REFERENCES `employee_employee` (`id`);

--
-- Constraints for table `employee_employee_uniform`
--
ALTER TABLE `employee_employee_uniform`
  ADD CONSTRAINT `employee_employee_un_employee_id_b1c06dc4_fk_employee_` FOREIGN KEY (`employee_id`) REFERENCES `employee_employee` (`id`);

--
-- Constraints for table `employee_employee_vale`
--
ALTER TABLE `employee_employee_vale`
  ADD CONSTRAINT `employee_employee_va_employee_id_82de8239_fk_employee_` FOREIGN KEY (`employee_id`) REFERENCES `employee_employee` (`id`);

--
-- Constraints for table `employee_employment_record`
--
ALTER TABLE `employee_employment_record`
  ADD CONSTRAINT `employee_employment__employee_id_038d4d31_fk_employee_` FOREIGN KEY (`employee_id`) REFERENCES `employee_employee` (`id`);

--
-- Constraints for table `hrms_company_rates`
--
ALTER TABLE `hrms_company_rates`
  ADD CONSTRAINT `hrms_company_rates_company_id_08aa257e_fk_hrms_company_id` FOREIGN KEY (`company_id`) REFERENCES `hrms_company` (`id`);

--
-- Constraints for table `payrolllist_base_payroll`
--
ALTER TABLE `payrolllist_base_payroll`
  ADD CONSTRAINT `payrolllist_base_payroll_company_id_c3b1740d_fk_hrms_company_id` FOREIGN KEY (`company_id`) REFERENCES `hrms_company` (`id`);

--
-- Constraints for table `payrolllist_payroll`
--
ALTER TABLE `payrolllist_payroll`
  ADD CONSTRAINT `payrolllist_payroll_base_payroll_id_66659f13_fk_payrollli` FOREIGN KEY (`base_payroll_id`) REFERENCES `payrolllist_base_payroll` (`id`),
  ADD CONSTRAINT `payrolllist_payroll_employee_id_9cc9eed8_fk_employee_employee_id` FOREIGN KEY (`employee_id`) REFERENCES `employee_employee` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
